﻿#pragma strict
function Start () 
{
    InvokeRepeating("setAspect",0, 0.8f);
}

function setAspect () 
{
	// set the desired aspect ratio (the values in this example are
    // hard-coded for 16:9, but you could make them into public
    // variables instead so you can set them at design time)
    var targetaspect : float = 16.0f / 9.0f;

    // determine the game window's current aspect ratio
    var windowaspect : float = float.Parse(Screen.width.ToString()) / float.Parse(Screen.height.ToString());

    // current viewport height should be scaled by this amount
    var scaleheight : float = windowaspect / targetaspect;

    // obtain camera component so we can modify its viewport
    var camera : Camera = Camera.main;
	var rect : Rect = camera.rect;
	
    // if scaled height is less than current height, add letterbox
    if (scaleheight < 1.0f)
    {  
        

        rect.width = 1.0f;
        rect.height = scaleheight;
        //rect.x = 0;
        //rect.y = (1.0f - scaleheight) / 2.0f;
        
        camera.rect = rect;
    }
    else // add pillarbox
    {
        var scalewidth : float = 1.0f / scaleheight;

        
        rect.width = scalewidth;
        rect.height = 1.0f;
        rect.x = (1.0f - scalewidth) / 2.0f;
        rect.y = 0;

        camera.rect = rect;
    }
}