﻿#pragma strict
public var startSh : float;
public var timerSh : float;

public var goTo : Vector3;
@HideInInspector
public var startF : Vector3;
function Start () 
{
	yield WaitForSeconds(startSh/2);
	startF = transform.position;
	goTo.z = transform.position.z;
	while(true)
	{
		transform.position = goTo;
		yield WaitForSeconds(0.001+timerSh/2);
		transform.position = startF;
		yield WaitForSeconds(0.001+timerSh/2);
	}
}

function Update () {
//set
}