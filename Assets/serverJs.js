﻿#pragma strict
import System.IO;
import System.Collections.Generic;
public var clients : List.<clientCl> = new List.<clientCl>();

public var lastInd : int = 0;
function Start () 
{
	openNewSocket();
	InvokeRepeating("readClients",0, 0.04f);
}
function openNewSocket()
{
	clients.Add(new clientCl());
	
	clients[lastInd].setupSocket(lastInd);
	lastInd++;
}
function clientDisconnected(ind : int)
{
	clients.RemoveAt(ind);
	Debug.Log("Disconnection handled");
	
	if(ind == 0)
	{
		Debug.Log("The last client tried to disconnect...");
		openNewSocket();
		Debug.Log("Re-opened socket");
	}
	lastInd = clients.Count+1;
}
function readClients()
{
	for(var client : clientCl in clients)
		client.readSocket();
}
function Update()
{
}