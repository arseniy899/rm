﻿#pragma strict
#if UNITY_EDITOR
@CustomEditor (move)
@CanEditMultipleObjects
public class points extends Editor
{
	public var show : boolean = true;
    function OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        var myScript : move = target;
        if(GUILayout.Button("Add point!"))
            myScript.addPoint();
        if(show && GUILayout.Button("Hide!"))
        	show = false;
        if(!show && GUILayout.Button("Show!"))
        	show = true;
        if(show)
        {
	       // var cords : List.<Vector2>;
			//cords = new List.<Vector2>();
	        for(var cord : Vector2 in myScript.cords)
	     	{	
	     		EditorGUILayout.BeginHorizontal();
	     		//@SerializableField
	           	cord = EditorGUILayout.Vector2Field("",cord);
	           	if (GUILayout.Button("X")) 
	     		 	myScript.cords.Remove(cord);
				if (myScript.gameObject.transform.position != cord && GUILayout.Button("MT")) 
	     		 	myScript.moveTo(cord);
	 		 	EditorGUILayout.EndHorizontal();
	        } 
        }
        if (GUI.changed)
				EditorUtility.SetDirty(myScript);
    }
}
#endif