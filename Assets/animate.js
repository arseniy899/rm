﻿#pragma strict
public var sprites : Sprite[];
public var spr_comp : SpriteRenderer;
public var timer_ms : float;
public var playing : boolean;
public var setActive : UI.Image;
public var playOnce : boolean = false;
public var playInderWay : boolean = false;
function Start () 
{
	//Debug.Log("Start()");
	spr_comp = gameObject.GetComponent(SpriteRenderer);
	
	while(playing)
	{
		for(var cur_sprite : Sprite in sprites)
		{
			spr_comp.sprite = cur_sprite;
			yield WaitForSeconds(0.001+(timer_ms/1000));
		}
		if(playInderWay)
		{
			for(var i=sprites.Length-1;i>=0;i--)
			{
				spr_comp.sprite = sprites[i];
				yield WaitForSeconds(0.001+(timer_ms/1000));
			}
		}
		if(timer_ms == 0)
			yield WaitForSeconds(0.001+(timer_ms/1000));
		if(playOnce)
			playing = false;
	}
	if(setActive  != null)
	{
		yield WaitForSeconds(0.001+(timer_ms/1000));
		setActive.gameObject.SetActive(true);
		for(var q=0;q<255;q+=15)
		{
			setActive.color.a = q;
			yield WaitForSeconds(0.09);
		}
	}
	//Debug.Log("Start() end");
}

function play () 
{
	playing = true;
	Start();
}
function stop()
{
	playing = false;
}