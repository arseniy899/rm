﻿#pragma strict
import System.Text.RegularExpressions;
function speedx (times : int) 
{
	Time.timeScale = times;
}
function loadScene (name : String) 
{
	Application.LoadLevel(name);
}
function loadNextLevel()
{
	var id : int = int.Parse(Regex.Replace(Application.loadedLevelName, "[^0-9]", ""));
	Application.LoadLevel("level"+(id+1));
}
function loadScene (id : int) 
{
	Application.LoadLevel(id);
}
function restartCurrentScene () 
{
	Application.LoadLevel(Application.loadedLevel);
}