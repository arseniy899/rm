﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;
using System.Net;
public class tcp : MonoBehaviour 
{
	bool socketReady = false;
	
	TcpClient mySocket;
	NetworkStream theStream;
	StreamWriter theWriter;
	StreamReader theReader;
	public String Host = "localhost";
	public Int32 Port = 13000; 
	byte [] buffer = new byte[1024];
	// Use this for initialization
	void Start() {
		
	}
	
	// Update is called once per frame
	void Update() {
		
	}
	
	public void setupSocket() {
		try {
			mySocket = new TcpClient(Host, Port);
			theStream = mySocket.GetStream();
			theWriter = new StreamWriter(theStream);
			theReader = new StreamReader(theStream);
			socketReady = true;
		}
		catch (Exception e) {
			Debug.Log("Socket error:" + e);
		}
	}
	
	public void writeSocket(string theLine) {
		if (!socketReady)
			return;
		String tmpString = theLine + "\r\n";
		theWriter.Write(tmpString);
		theWriter.Flush();
	}
	
	public String readSocket() {
		if (!socketReady)
			return "";
		//Debug.Log ("Begin reading: ");
		String str;
		Debug.Log ("theStream.DataAvailable: "+theStream.DataAvailable);
		if (theStream.DataAvailable) {
			/*theReader.BaseStream.Read (buffer, 0, 1024);
			str =  System.Text.Encoding.Default.GetString (buffer);*/
			str = theReader.ReadLine();
			Debug.Log ("Income string: "+str);
			return str;
		}
		return "";
	}

	public void closeSocket() {
		if (!socketReady)
			return;
		theWriter.Close();
		theReader.Close();
		theStream.Dispose();
		mySocket.Close();
		socketReady = false;
	}
	public static string getIPfURL(string aURL)
	{
		IPHostEntry Hosts = Dns.GetHostEntry(aURL);
		return Hosts.AddressList[0].ToString();
	}
	public void maintainConnection(){
		if(!theStream.CanRead) {
			setupSocket();
		}
	}
} // end class s_TCP