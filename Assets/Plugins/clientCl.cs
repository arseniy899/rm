﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;

public class clientCl : MonoBehaviour 
{
	bool socketReady = false;
	
	TcpListener mySocket;
	NetworkStream theStream;
	StreamWriter theWriter;
	StreamReader theReader;
	public TcpClient client;
	StreamReader reader;
	StreamWriter writer;
	NetworkStream ns;
	public String Host = "localhost";
	public Int32 Port = 13000; 
	byte [] buffer = new byte[1024];
	public GameObject back;
	public int index = 0;
	// Use this for initialization
	void Start() {
		
	}
	
	// Update is called once per frame
	void Update() 
	{
		//readSocket ();
		/*if ((client != null) && !client.Connected) 
		{
			client = null;
			Debug.Log ("client disconnected");
		}*/

	}
	public bool IsConnected()
	{
		return true;
	}
	public void setupSocket(int ind) 
	{
		index = ind;
		try {
			mySocket = new TcpListener(Port);

			//theStream = ;
			mySocket.Start();
			//theWriter = new StreamWriter(theStream);
			//theReader = new StreamReader(theStream);
			socketReady = true;


		}
		catch (Exception e) {
			Debug.Log("Socket error:" + e);
		}

		//mySocket.AcceptTcpClient();
	}
	public void writeSocket(string theLine) 
	{
		if (!socketReady || theLine == "\r\n" || client == null)
			return;
		if (theLine.Contains ("GET") || theLine.Contains ("HTTP") || theLine.Contains ("POST"))
			return;
		if (!theLine.Contains ("*") && !theLine.Contains ("#"))
			return;
		string tmpString = theLine + "\r\n";
		try
		{
			writer.WriteLine(tmpString);
			writer.Flush();
		}
		catch (Exception e) 
		{
			client = null;
			//Debug.Log("Socket error:" + e.Source);
			//if(e.Source == "Socket error:System")
			Debug.Log("client disconnected"+client);
			client = null;
			back.SendMessage("clientDisconnected");
		}
	}

	public void writeSocket(string theLine, Boolean fromServer) 
	{
		if (fromServer == true) 
		{
			string tmpString = theLine + "\r\n";
			try
			{
				writer.WriteLine(tmpString);
				writer.Flush();
			}
			catch (Exception e) 
			{
				client = null;
				//Debug.Log("Socket error:" + e.Source);
				//if(e.Source == "Socket error:System")
				Debug.Log("client disconnected"+client);
				client = null;
				back.SendMessage("clientDisconnected");
			}

		}

	}
	public void readSocket() 
	{
		if (!mySocket.Pending() )
		{
			if(client != null && ns.DataAvailable)
			{
				string msg;
				msg = reader.ReadLine();
				if(msg.Length==0 || msg=="0")
				{
					back.SendMessage("clientDisconnected");
					Debug.Log("client disconnected");
				}
				if(msg.Length>0)
					parseMsg(msg);
			}
			if ((client != null) && !client.Connected)//!client.Connected) 
			{
				client = null;
				Debug.Log ("client disconnected");
				back.SendMessage("clientDisconnected");
			}
		}
		else
		{
			if(client == null)
			{
				client = mySocket.AcceptTcpClient();
				ns = client.GetStream();
				reader = new StreamReader(ns);
				writer = new StreamWriter(ns);

				back.SendMessage("openNewSocket");
				Debug.Log("Client connected");
			}
		}
	}
	
	public void closeSocket() {
		if (!socketReady)
			return;
		theWriter.Close();
		theReader.Close();
		mySocket.Stop();
		client = null;
		socketReady = false;
	}
	public void OnConnectionLost()
	{
		client = null;
		back.SendMessage("clientDisconnected");
		Debug.Log ("client_disconnected");
	}
	public void parseMsg(string msg)
	{
		if (msg.Contains ("regNew")) 
		{
			//string 
		}
	}

} // end class s_TCP