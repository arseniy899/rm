﻿#pragma strict
import System.Text.RegularExpressions;
public var show : GameObject;
public var sprite : SpriteRenderer;
public var sRobot  : robot;
public var levelId : int;
function Start () 
{
	levelId = int.Parse(Regex.Replace(Application.loadedLevelName, "[^0-9]", ""));
	while(false)
	{
		sprite.color.a = 0;
		yield WaitForSeconds(0.5);
		sprite.color.a = 255;
		yield WaitForSeconds(0.5);
	}
}

function OnTriggerEnter2D (go : Collider2D)
{
	if(go.gameObject.tag == "Player")
	{
		show.SetActive(true);
		var prog : String = PlayerPrefs.GetString("progress");
		if(prog.Contains(levelId+":"))
		{
			var level_pos : int = prog.IndexOf(levelId+":")+2;
			var prev_level_stars : int = (prog[level_pos]);
			if(prev_level_stars < sRobot.stars)
			{
				prog = prog.Substring (0, level_pos) + sRobot.stars.ToString() + prog.Substring (level_pos, level_pos + 1);
				Debug.Log(prog);
				PlayerPrefs.SetInt("stars", PlayerPrefs.GetInt("stars")+(sRobot.stars - prev_level_stars));
			}
		}
		else
			PlayerPrefs.SetString("progress",prog+levelId+":"+sRobot.stars+";");
		
		
	}
}