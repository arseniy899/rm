﻿#pragma strict
//@script SerializePublicVariables;
@script ExecuteInEditMode();

//var objects = level_editor_window ();

class level_editor_window extends EditorWindow 
{
	@SerializeField
	public var points : List.<Vector2> = new List.<Vector2>();
	public var collid : PolygonCollider2D;
	public var sprit : Sprite;
	//@level_editor_window Serializable
	public var recOffset : Vector2;
	public var recSize : Vector2;
	//@MenuItem ("Window/Level editor")
	@MenuItem ("Window/Level collider generator")
	static function Init () 
	{
		// Get existing open window or if none, make a new one:		
		var window = ScriptableObject.CreateInstance.<level_editor_window>();
		window.Show();
		window.autoRepaintOnSceneChange = true;
		//window.name="Editor";
		//create_map();
	}
	function OnEnable()
	{
		//Init();
		
		title = "Level Editor";
	}
	function convXYTOA(x : int, y : int)
	{
		return x*recSize.x + recSize.y-y;
	}
	function generate()
	{
		points.Clear();
		collid.SetPath(0, points.ToArray());
		recOffset = new Vector2(sprit.textureRect.x, sprit.textureRect.y);
		recSize = new Vector2(sprit.textureRect.width, sprit.textureRect.height);
		var pix : Color[]= sprit.texture.GetPixels(recOffset.x, recOffset.y, recSize.x, recSize.y);
		//var pix : Color32[]= text.GetPixels32();
		System.Array.Reverse(pix);
		
		
		Debug.Log(recSize.x+";"+recSize.y);
		Debug.Log(recOffset.x+";"+recOffset.y);
		//Debug.Log(sprit.textureRect.x+";"+sprit.textureRect.y);
		Debug.Log(sprit.textureRect.width+";"+sprit.textureRect.height);
		for(var x =1;x<recSize.x-1;x+=1)
		{
			for(var y =1;y<recSize.y-1;y+=1)
			{
				var left : Color = pix[convXYTOA(x-1,y)];
				var right : Color = pix[convXYTOA(x+1,y)];
				var up : Color = pix[convXYTOA(x,y+1)];
				var down : Color = pix[convXYTOA(x,y-1)];
				var countW : int = 0;var countB : int = 0;
				if(left.a > 0)
					countW++;
				else
					countB++;
				if(right.a > 0)
					countW++;
				else
					countB++;
				if(up.a > 0)
					countW++;
				else
					countB++;
				if(down.a > 0)
					countW++;
				else
					countB++;
				if(countW == 3 || countB == 3)
				{
					points.Add(new Vector2(x/100f,y/100f));
					//Debug.Log(x+";"+y+" "+points.Count);
				}
				//Debug.Log(countB+";"+countW);
				//Debug.Log(x+";"+y+";");
			}
			//Debug.Log(x);
		}
		
		
		collid.SetPath(0, points.ToArray());
	}
	function OnGUI () 
	{
		collid = EditorGUILayout.ObjectField("Collider2D: ",collid, typeof( PolygonCollider2D ) );
		sprit = EditorGUILayout.ObjectField("Sprite: ",sprit, typeof( Sprite ) );
		if (GUILayout.Button("Generate")) 
     		generate();
        //Repaint();
	}
}
/*class add_obj_win extends EditorWindow {
	var go : GameObject;
	static var window : add_obj_win;
	//@MenuItem ("Window/Level editor")
	static function add_object () 
	{
		// Get existing open window or if none, make a new one:		
		window = ScriptableObject.CreateInstance.<add_obj_win>();
		window.Show();
	}
	function OnGUI () 
	{
		
		GUILayout.Label ("Add an object", EditorStyles.boldLabel);
		if(go !=null)
			go = EditorGUILayout.ObjectField(go.name, go, typeof( GameObject ) );
		else
			go = EditorGUILayout.ObjectField("", go, typeof( GameObject ) );
		if(go != null && !go.name.Contains("$"))
		{
	     	if (GUILayout.Button("Add")) 
	     	{
	            level_editor_window.objects.Add(go);
	            window.Close();
	     	}
     	}
     	else if(go == null) 
     		GUILayout.Label ("Please assign the object!!", EditorStyles.label);
     	else if(go.name.Contains("$")) 
     		GUILayout.Label ("Error: Please consider, that name doesn't contain '$'!", EditorStyles.label);
     	
	}
}*/