﻿#pragma strict
	import System.Collections.Generic;
@ExecuteInEditMode
@System.Serializable
@HideInInspector

public class move extends MonoBehaviour
{
	@SerializeField
	public var cords : List.<Vector2>;
		//cords = new List.<Vector2>();
	public var runInf : boolean = false;
	public var rotatable : boolean = false;
	public var speed : float = 1;
	public var awaitTimeEnd : float = 0;
	public var awaitTimePoint : float = 0;
	public var rigid : Rigidbody2D;
	function Start () 
	{
		yield run();
		yield WaitForSeconds(0.001+awaitTimeEnd/2);
		while(runInf)
		{
			yield run();
			yield WaitForSeconds(0.001+awaitTimeEnd/2);
		}
	}
	function moveTo(pos : Vector2)
	{
		transform.position = pos;
	}
	function run () 
	{
		transform.position = cords[0];
		for(var i=0;i<cords.Count;i++)
		{
			var pos : Vector2 = cords[i];
			var begin_pos : Vector2 = transform.position;
			var angle = Mathf.Round(robot.Atan2((pos.x-begin_pos.x),(pos.y-begin_pos.y))*180/3.14);
	        
			rigid.velocity.x = Mathf.Cos(angle*3.14/180) * speed;
			rigid.velocity.y = Mathf.Sin(angle*3.14/180) * speed;
			
			if(rotatable)
			{
				transform.LookAt(pos);
				transform.localRotation.x = 0;
				transform.localRotation.y = 0;
			}
			while((Time.timeScale <= 1 && Vector2.Distance(transform.position, pos) > 0.03) || (Time.timeScale > 1 && Vector2.Distance(transform.position, pos) > 0.15))
			{
				if(Vector2.Distance(transform.position, pos) > 10)
					return;
				/*if(Mathf.Abs(rigid.velocity.x) <= 0.5 && Mathf.Abs(rigid.velocity.y) <= 0.5)
				{
					rigid.velocity.x = Mathf.Cos(angle*3.14/180) * speed;
					rigid.velocity.y = Mathf.Sin(angle*3.14/180) * speed;
				}*/
				yield WaitForSeconds(0.01);
			}
			rigid.velocity = Vector2.zero;
			if((i+1)<cords.Count)
				yield WaitForSeconds(0.001+awaitTimePoint/2);
		}
		//rigid.velocity = Vector2.zero;
		
	}
	function addPoint()
	{
		cords.Add(transform.position);
	}
}