﻿#pragma strict
public var path : tracer;
public var rigid : Rigidbody2D;
public var anim : animate;
private var goY : boolean = false;
private var stop : boolean = false;
private var i : int = 0;
public var stars : int;
private var starsGo : List.<GameObject>;
	starsGo = new List.<GameObject>();

function Start () 
{
	Application.targetFrameRate = 120;
	Time.timeScale = 1;
	for(var go : GameObject in GameObject.FindGameObjectsWithTag("star"))
		starsGo.Add(go);
	path.sendCords(transform.position);
	while(true)
	{
		if(goY)
		{
			anim.play();
			Debug.Log(path.cords[0]);
			transform.position = path.cords[0];
			for(i=0;i<path.cords.Count;i++)
			{
				var pos : Vector2 = path.cords[i];
				var begin_pos : Vector2 = transform.position;
				var angle = Mathf.Round(Atan2((pos.x-begin_pos.x),(pos.y-begin_pos.y))*180/3.14);
		        
				rigid.velocity.x = Mathf.Cos(angle*3.14/180) * 1;
				rigid.velocity.y = Mathf.Sin(angle*3.14/180) * 1;
				var angles : float;
				if((4+i) < path.cords.Count && i > 4)
				{
					for(var q = -4;q<4;q++)
						angles+= Mathf.Round(Atan2((path.cords[q+i].x-begin_pos.x),(path.cords[q+i].y-begin_pos.y))*180/3.14);
					angles /= q;
				}
				
				//Debug.Log(angles);
				//transform.Rotate(0, 0, angles);
				//transform.rotation.z = angle;
				
				//if((3+i) < path.cords.Count)
					transform.LookAt(path.cords[i], transform.forward);
					
				transform.localRotation.x = 0;
				transform.localRotation.y = 0;
				if(stop)
				{
					stop = false;
					i = 0;
					transform.position = path.cords[0];
					transform.position.z=-1;
					Debug.Log("restarted moving");
					rigid.velocity.x = 0;
					rigid.velocity.y = 0;
					
				}
				else
					while(Vector2.Distance(transform.position, pos) > 0.2)
					{
						//Debug.Log(rigid.velocity.x + " x : y "+ rigid.velocity.y);
						if(Mathf.Abs(rigid.velocity.x) <= 0.5 && Mathf.Abs(rigid.velocity.y) <= 0.5)
						{
							rigid.velocity.x = Mathf.Cos(angle*3.14/180) * 1;
							rigid.velocity.y = Mathf.Sin(angle*3.14/180) * 1;
							/*i = 0;
							transform.position = path.cords[0];
							Debug.Log("restarted moving bcs of stop");
							rigid.velocity.x = 0;
							rigid.velocity.y = 0;
							break;*/
						}
						if(stop)
							break;
						if(!goY)
							break;
						yield WaitForSeconds(0.01);
					}
				
			}
			//anim.stop();
			transform.position = path.cords[0];
			transform.position.z=-1;
			goY = false;
			rigid.velocity.x = 0;
			rigid.velocity.y = 0;
		}
		yield WaitForSeconds(0.07);
	}
}

function go () 
{
	onRobotReset();
	goY = true;
}
function setSpeed(angle : float)
{
	var velX = Mathf.Cos(angle*3.14/180) * 5;
	var velY = Mathf.Sin(angle*3.14/180) * 5; 
	rigid.velocity.x = velX;
	rigid.velocity.y = velY;
	yield WaitForSeconds(0.1);
}
static function Atan2(x : float , y : float):float
{
	if(x == 0 && y == 0)
		return 0;
	var a = Mathf.Acos(x / Mathf.Sqrt(x*x + y*y));
	return y >= 0 ? a : -a;
}
function onRobotReset()
{
	transform.position = path.cords[0];
	stop = true;
	goY = false;
	rigid.velocity.x = 0;
	rigid.velocity.y = 0;
	transform.localRotation.z = 0;
	
	for(var go : GameObject in starsGo)
	{
		go.SetActive(true);
		go.SendMessage("play");
	}
	stars = 0;
}
function OnTriggerEnter2D (go : Collider2D)
{
	if(go.gameObject.tag == "kill")
		onRobotReset();
	if(go.gameObject.tag == "star")
	{
		stars++;
		go.gameObject.SetActive(false);
	}
}