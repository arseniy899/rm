﻿#pragma strict
import System.Collections.Generic;
public var cords : List.<Vector2>;
	cords = new List.<Vector2>();
public var objs : List.<GameObject>;
	objs = new List.<GameObject>();
public var prevPos : Vector2;
//@HideInInspector
public var lastClearedInd : int = -1;
public var tracerPrefab : GameObject;
public var notConnected : boolean = false;
public var disInd : int;
public var cleaning : boolean = false;
function Atan2(x : float , y : float):float
{
	if(x == 0 && y == 0)
		return 0;
	var a = Mathf.Acos(x / Mathf.Sqrt(x*x + y*y));
	return y >= 0 ? a : -a;
}

function Update () 
{
	if((Input.GetMouseButton(0)&& !Input.GetMouseButtonUp(0)) || Input.touchCount > 0)
	{
		var curPos : Vector2;
		if(Input.touchCount > 0)
		{
			for (var touch : Touch in Input.touches) 
			{
				if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
					curPos = Camera.main.ScreenToWorldPoint(touch.position);
			}
			
		}
		else //if(Input.GetMouseButton(0))
			curPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		//curPos.x /=500;
		//curPos.y /=500;
		
		if(!cleaning && Vector2.Distance(curPos, prevPos) > 0.5 && Vector2.Distance(curPos, prevPos) < 1.3)// && curPos.y > -4.0)
		{
			var go : GameObject = GameObject.Instantiate(tracerPrefab, curPos, Quaternion.identity);
			
			//var angle = Mathf.Round(Atan2((curPos.x-prevPos.x),(curPos.y-prevPos.y))*180f/3.14);
			var angle = Vector3.Angle(curPos, prevPos)*180f/3.14 / 10;
			if(curPos.y < prevPos.y)
				angle +=180;
	        
	        go.transform.LookAt(prevPos, go.transform.forward);
	        
	        go.transform.RotateAround(go.transform.forward, 135);
	        go.transform.localRotation.x = 0;go.transform.localRotation.y = 0;
	        go.transform.parent = transform;
	        
			//Debug.Log(Vector3.Angle(curPos, prevPos)*180f/3.14 / 10);
			/*var line : LineRenderer = go.GetComponent(LineRenderer);
			line.SetPosition(0, curPos);
	 		line.SetPosition(1, prevPos);
	 		*/
			prevPos = curPos;
			if(!notConnected)
			{
				cords.Add(curPos);
				go.name = (cords.Count-1)+"_part";
				objs.Add(go);
			}
			else
			{
				go.name = disInd+"_part";
				if((disInd+1) < cords.Count && Vector2.Distance(cords[disInd+1], curPos) < 0.8)
				{
					lastClearedInd =-1;
					notConnected = false;
				}
				if(disInd < cords.Count)
				{
					cords.Insert(disInd, curPos);
					objs.Insert(disInd, go);
				}
				else
				{
					cords.Add(curPos);
					objs.Add(go);
				}
				disInd++;
			}
		}
		if(cleaning)
		 removeAt(curPos);
	}
	#if UNITY_EDITOR
		//Debug.Log(Camera.main.ScreenToWorldPoint(Input.mousePosition));
	#endif
}
function clearSwitch(b : UI.Toggle)
{
	cleaning = b.isOn;
	/*if(cleaning)
		cleaning = false;
	else
		cleaning = true;*/
}
function removeAt(pos : Vector2)
{
	
	for(var i = 0; i< cords.Count; i++)
	{
		
		if(Vector2.Distance(cords[i], pos) < 1 && (Mathf.Abs(lastClearedInd - i)<2 || lastClearedInd == -1))
		{
			
			lastClearedInd = i;
			Debug.Log(lastClearedInd);
			if(i< objs.Count && objs[i] != null)
				Destroy(objs[i]);
			cords.RemoveAt(i);
			if(i< objs.Count)
				objs.RemoveAt(i);
			prevPos = cords[i];
			if((i+1) < cords.Count)
				notConnected = true;
		}
	}
	for(i = 0; i<(cords.Count-1); i++)
	{
		if(Vector2.Distance(cords[i], cords[i+1]) > 1.5 || (i+2) == cords.Count)
		{
			disInd = i+1;
			prevPos = cords[i];
			
			break;
		}
	}
}
function removeLast()
{
	for(var i =0; i<1;i++)
	{
		if(cords.Count <= 1)
			return;
		prevPos = cords[cords.Count-1];
		cords.RemoveAt(cords.Count-1);
		objs.RemoveAt(objs.Count-1);
		Destroy(transform.FindChild((cords.Count)+"_part").gameObject);
		
	}
	
}
function sendCords(pos : Vector2 )
{
	prevPos = pos;
	yield WaitForSeconds(0.1);
	cords.Add(pos);
}
function clear()
{
	prevPos = cords[0];
   	for(var child : Transform in transform)
             GameObject.Destroy(child.gameObject);
	cords.Clear();
	cords.Add(prevPos);
	//GameObject.FindGameObjectWithTag("Player").SendMessage("onRobotReset");
}